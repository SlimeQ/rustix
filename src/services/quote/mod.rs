mod backend;
mod models;

mod quotes;
mod del_quote;

// Re-export
pub use quotes::Quotes;
pub use del_quote::DelQuote;
